CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Fakelink module allows you to create '#' links for use with javascript functions. Simply enter `#fakelink` as your link and it will transform your href to `#`.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/fakelink
 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/fakelink

RECOMMENDED MODULES
-------------------

 * No extra module is required.

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
   information.

CONFIGURATION
-------------

 * No configuration is needed.


MAINTAINERS
-----------

Current maintainers:

 * Travis Neilans (porchlight) (https://www.drupal.org/user/2623705/)
