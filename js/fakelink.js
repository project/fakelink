/**
 * @file
 * The fakelink integration.
 */

(function($) {
  Drupal.behaviors.fakelink = {
    attach: function (context, settings) {
      $('a[href$="#fakelink"]').each(function() {
        $(this).attr('href', '#');
        $(this).attr('data-link', 'fakelink');
      });
      $('a[data-link$="fakelink"]', context).once().click(function(e) {
        e.preventDefault();
      });
    }
  };
})(jQuery);
